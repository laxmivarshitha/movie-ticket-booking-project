#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_MOVIES 5
#define MAX_ROWS 5
#define MAX_COLS 10

struct Movie {
    char title[50];
    int duration; 
    int available_seats[MAX_ROWS][MAX_COLS];
    int total_seats;
    char payment_method[20];
};

struct Booking {
    char user_name[50];
    char movie_title[50];
    int num_seats;
    int rows[MAX_ROWS * MAX_COLS];
    int cols[MAX_ROWS * MAX_COLS];
    int amount_due;
    char payment_method[20];
};

void initializeMovies(struct Movie movies[]) {
    // Initialize movie data
    strcpy(movies[0].title, "Inception");
    movies[0].duration = 150;
    movies[0].total_seats = MAX_ROWS * MAX_COLS;
    strcpy(movies[0].payment_method, "Credit Card");
    
    strcpy(movies[1].title, "The Dark Knight");
    movies[1].duration = 152;
    movies[1].total_seats = MAX_ROWS * MAX_COLS;
    strcpy(movies[1].payment_method, "PayPal");
    
    strcpy(movies[2].title, "Angry Birds");
    movies[2].duration = 150;
    movies[2].total_seats = MAX_ROWS * MAX_COLS;
    strcpy(movies[2].payment_method, "Credit Card");
    
    strcpy(movies[3].title, "Captain Marvel");
    movies[3].duration = 140;
    movies[3].total_seats = MAX_ROWS * MAX_COLS;
    strcpy(movies[3].payment_method, "Credit Card");
    
    strcpy(movies[4].title, "Avengers End Game");
    movies[4].duration = 172;
    movies[4].total_seats = MAX_ROWS * MAX_COLS;
    strcpy(movies[4].payment_method, "Ppay");
    
    for (int i = 0; i < MAX_MOVIES; ++i) {
        for (int row = 0; row < MAX_ROWS; ++row) {
            for (int col = 0; col < MAX_COLS; ++col) {
                movies[i].available_seats[row][col] = 1; 
            }
        }
    }
}

void displayMovies(struct Movie movies[]) {
    printf("\nAvailable Movies:\n");
    for (int i = 0; i < MAX_MOVIES; ++i) {
        printf("%d. %s (Duration: %d minutes, Available Seats: %d/%d)\n", 
               i + 1, movies[i].title, movies[i].duration, movies[i].total_seats, 
               movies[i].total_seats - (MAX_ROWS * MAX_COLS - movies[i].total_seats));
    }
}

void displaySeats(int seats[MAX_ROWS][MAX_COLS]) {
    printf("\nAvailable Seats:\n");
    for (int row = 0; row < MAX_ROWS; ++row) {
        for (int col = 0; col < MAX_COLS; ++col) {
            printf("%c ", seats[row][col] == 1 ? 'O' : 'X'); // 'O' represents available seat, 'X' is booked
        }
        printf("\n");
    }
}

void displayBookingSummary(struct Booking bookings[], int numBookings) {
    printf("\nBooking Summary:\n");
    for (int i = 0; i < numBookings; ++i) {
        printf("User: %s,\n Movie: %s,\n Seats: ", bookings[i].user_name, bookings[i].movie_title);
        for (int j = 0; j < bookings[i].num_seats; ++j) {
            printf("Row %d, Col %d \n", bookings[i].rows[j] + 1, bookings[i].cols[j] + 1);
            if (j < bookings[i].num_seats - 1) {
                printf(", ");
            }
        }
        printf("Amount Due: $%d\n Payment Method: %s\n", bookings[i].amount_due, bookings[i].payment_method);
    }
}

int main() {
    struct Movie movies[MAX_MOVIES];
    struct Booking bookings[MAX_MOVIES * MAX_ROWS * MAX_COLS];
    int numBookings = 0;

    initializeMovies(movies);

    int userChoice, selectedMovie;
    do {
        printf("\nMovie Ticket Booking System Menu:");
        printf("\n1. Display Available Movies");
        printf("\n2. Book a Ticket");
        printf("\n3. Display Booking Summary");
        printf("\n4. Exit");
        printf("\nEnter your choice: ");
        scanf("%d", &userChoice);

        switch (userChoice) {
            case 1:
                // Display available movies along with the number of available seats
                displayMovies(movies);
                break;
            case 2:
                // Book a ticket
                displayMovies(movies);
                printf("\nEnter the movie number you want to watch: ");
                scanf("%d", &selectedMovie);

                if (selectedMovie >= 1 && selectedMovie <= MAX_MOVIES) {
                    // Additional input: Display available seats for the selected movie
                    printf("\nAvailable Seats for %s:\n", movies[selectedMovie - 1].title);
                    displaySeats(movies[selectedMovie - 1].available_seats);

                    char userName[50];
                    printf("\nEnter your name: ");
                    scanf("%s", userName);

                    int selectedRow, selectedCol, numSeats;
                    printf("\nEnter the number of seats you want to book: ");
                    scanf("%d", &numSeats);

                    printf("\nEnter the row and column number for each seat:\n");
                    for (int i = 0; i < numSeats; ++i) {
                        printf("Seat %d: ", i + 1);
                        scanf("%d %d", &selectedRow, &selectedCol);

                        if (selectedRow >= 1 && selectedRow <= MAX_ROWS && selectedCol >= 1 && selectedCol <= MAX_COLS &&
                            movies[selectedMovie - 1].available_seats[selectedRow - 1][selectedCol - 1] == 1) {
                            // Book the seat
                            movies[selectedMovie - 1].available_seats[selectedRow - 1][selectedCol - 1] = 0;
                            bookings[numBookings].rows[i] = selectedRow - 1;
                            bookings[numBookings].cols[i] = selectedCol - 1;
                        } else {
                            printf("\nInvalid seat selection. Please choose an available seat.\n");
                            --i; // Retry the same seat
                        }
                    }

                    strcpy(bookings[numBookings].user_name, userName);
                    strcpy(bookings[numBookings].movie_title, movies[selectedMovie - 1].title);
                    bookings[numBookings].num_seats = numSeats;
                    strcpy(bookings[numBookings].payment_method, movies[selectedMovie - 1].payment_method);

                   
                    bookings[numBookings].amount_due =  350 * numSeats;


                    printf("\nSeats booked successfully! Amount Due: $%d\n", bookings[numBookings].amount_due);

                    // Ask to continue booking or proceed to payment
                    int continueBooking;
                    printf("\nDo you want to continue booking? (1 for booking other tickets, 0 for proceed to payment): ");
                    scanf("%d", &continueBooking);

                    if (continueBooking == 0) {
                        // Proceed to payment
                        int paidAmount;
                        printf("\nEnter the amount you want to pay: $");
                        scanf("%d", &paidAmount);

                        if (paidAmount >= bookings[numBookings].amount_due) {
                            printf("\nPayment successful! Enjoy the movie!\n");
                            // Update the booking summary with the paid amount
                            bookings[numBookings].amount_due = 0;
                            ++numBookings;
                        } else {
                            printf("\nInsufficient payment. Remaining amount due: $%d\n", bookings[numBookings].amount_due - paidAmount);
                            // Retry payment or exit
                            printf("Do you want to retry payment? (1 for Yes, 0 for No): ");
                            scanf("%d", &continueBooking);

                            if (continueBooking == 0) {
                                ++numBookings;
                            }
                        }
                    } else {
                        ++numBookings;
                    }
                } else {
                    printf("\nInvalid movie number. Please enter a valid movie number.\n");
                }
                break;
            case 3:
                // Display booking summary
                displayBookingSummary(bookings, numBookings);
                break;
            case 4:
                // Exit the program
                printf("\nExiting Movie Ticket Booking System. Thank you!\n");
                break;
            default:
                printf("\nInvalid choice. Please enter a valid option.\n");
        }
    } while (userChoice != 4);

    return 0;
}
